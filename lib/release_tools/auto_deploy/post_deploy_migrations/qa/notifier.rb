# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module PostDeployMigrations
      module Qa
        # Notifier sends notifications for failed downstream QA pipelines.
        class Notifier
          include ::SemanticLogger::Loggable
          include AutoDeploy::QaNotifier

          def initialize(pipeline_id:, environment:)
            @pipeline_id    = pipeline_id
            @environment    = environment
            @qa_smoke_jobs  = ["qa:smoke:post_deploy_migrations:gstg"]
          end
        end
      end
    end
  end
end
