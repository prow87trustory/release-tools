# frozen_string_literal: true

module ReleaseTools
  module Services
    # ApproveService approves a merge request.
    class ApproveService
      include ::SemanticLogger::Loggable

      InvalidMergeRequestError = Class.new(ArgumentError)
      SelfApprovalError = Class.new(ArgumentError)

      attr_reader :client, :merge_request

      # Initialize the service
      #
      # @param merge_request [ReleaseTools::MergeRequest] the merge request to approve
      # @param token [string] the GitLab private token, it should not belong to the merge request's author
      # @param endpoint [string] the GitLab API endpoint
      def initialize(merge_request, token:, endpoint: GitlabClient::DEFAULT_GITLAB_API_ENDPOINT)
        @merge_request = merge_request
        @client = Gitlab::Client.new(endpoint: endpoint, private_token: token, httparty: GitlabClient.httparty_opts)
      end

      def execute
        Retriable.with_context(:api) do
          validate!

          if approved?
            logger.info('MR already approved', merge_request: merge_request.url)
            return
          end

          approve
        end
      end

      def approved?
        client.merge_request_approvals(project_id, iid).approved
      end

      def approve
        logger.info('Approving merge request', merge_request: merge_request.url)

        client.approve_merge_request(project_id, iid) unless SharedStatus.dry_run?
      end

      # Validates the merge request exists and the current user can approve it.
      #
      # @raise [InvalidMergeRequestError] when the merge request does not exist
      # @raise [SelfApprovalError] when the approver is the author
      def validate!
        author = merge_request.author&.id
        raise InvalidMergeRequestError if author.nil?

        current_user = client.user.id
        raise SelfApprovalError if author == current_user
      end

      private

      def project_id
        merge_request.project_id
      end

      def iid
        merge_request.iid
      end
    end
  end
end
