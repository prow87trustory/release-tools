# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PipelineTracer::MetricsService do
  describe '.from_pipeline_url' do
    it 'creates Service instance' do
      url = 'https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines/1590941'

      instance = described_class.from_pipeline_url(url, { version: 'pipeline 1', depth: 2 })

      pipeline = instance.instance_variable_get(:@pipeline)
      version = instance.instance_variable_get(:@version)
      depth = instance.instance_variable_get(:@depth)

      expect(pipeline.gitlab_instance).to eq('ops.gitlab.net')
      expect(pipeline.pipeline_id).to eq('1590941')
      expect(pipeline.project).to eq('gitlab-org/release/tools')
      expect(version).to eq('pipeline 1')
      expect(depth).to eq(2)
    end
  end

  describe '#execute' do
    subject(:instance) { described_class.new(pipeline: pipeline, version: version, depth: depth) }

    let(:pipeline) do
      ReleaseTools::PipelineTracer::Pipeline.new('ops.gitlab.net', 'gitlab-org/release/tools', '1590941')
    end

    let(:depth) { 2 }
    let(:version) { '15.10.202303221120-355b04fb8d1.936857331ed' }
    let(:metrics_client) { instance_double(ReleaseTools::Metrics::Client) }
    let(:pipeline_duration_labels) { "gitlab-org/release/tools,#{version}" }
    let(:job_duration_labels) { "job1,success,gitlab-org/release/tools,#{version}" }
    let(:jobs) { [build(:job, :success, name: 'job1'), build(:job, :running, name: 'job2')] }
    let(:bridge_jobs) { [build(:bridge_job, :success, name: 'bridge_job1')] }
    let(:job) { instance_double(ReleaseTools::PipelineTracer::Job, completed?: true, name: jobs[0].name, status: jobs[0].status) }

    before do
      allow(pipeline).to receive(:real_time_duration).and_return(200)
      allow(ReleaseTools::PipelineTracer::Job).to receive(:new).and_call_original
      allow(ReleaseTools::PipelineTracer::Job).to receive(:new).with(jobs[0], ReleaseTools::GitlabOpsClient).and_return(job)
      allow(job).to receive(:real_time_duration).and_return(300)

      allow(ReleaseTools::Metrics::Client).to receive(:new).and_return(metrics_client)

      allow(pipeline)
        .to receive(:jobs)
        .and_return(Gitlab::PaginatedResponse.new(jobs))

      allow(pipeline)
        .to receive(:bridge_jobs)
        .and_return(Gitlab::PaginatedResponse.new(bridge_jobs))

      allow(described_class)
        .to receive(:from_pipeline_url)
        .and_return(double(execute: nil))

      allow(metrics_client).to receive(:set)
    end

    it 'creates metrics for pipeline' do
      expect(metrics_client)
        .to receive(:set)
        .with('deployment_pipeline_duration_seconds', pipeline.real_time_duration, labels: pipeline_duration_labels)

      instance.execute
    end

    it 'creates metric for jobs' do
      expect(metrics_client)
        .to receive(:set)
        .with('deployment_job_duration_seconds', job.real_time_duration, labels: job_duration_labels)

      expect(metrics_client)
        .not_to receive(:set)
        .with('deployment_job_duration_seconds', anything, labels: include("job2"))

      instance.execute
    end

    it 'creates metric for bridge jobs' do
      expect(described_class)
        .to receive(:from_pipeline_url)
        .with(bridge_jobs[0].downstream_pipeline.web_url, { version: version, depth: 1 })
        .and_return(double(execute: nil))

      instance.execute
    end

    context 'when depth is invalid' do
      let(:depth) { 4 }

      it 'raises error' do
        expect { instance.execute }.to raise_error(described_class::InvalidDepthError, 'Depth must be between 0 and 3')
      end
    end

    context 'when depth is 0' do
      subject(:instance) { described_class.new(pipeline: pipeline, version: version, depth: 0) }

      it 'does not create job duration metric' do
        expect(metrics_client)
          .to receive(:set)
          .with('deployment_pipeline_duration_seconds', pipeline.real_time_duration, labels: pipeline_duration_labels)

        expect(metrics_client).not_to receive(:set).with('deployment_job_duration_seconds', any_args)

        expect(described_class).not_to receive(:from_pipeline_url)

        instance.execute
      end
    end
  end
end
