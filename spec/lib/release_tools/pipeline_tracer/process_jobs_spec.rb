# frozen_string_literal: true

require 'spec_helper'
require 'opentelemetry/sdk'
require 'opentelemetry/exporter/otlp'

describe ReleaseTools::PipelineTracer::ProcessJobs do
  let(:jobs) do
    [
      build(:job,
            :success,
            name: 'job1',
            allow_failure: true,
            started_at: '2022-12-07T22:51:42.229Z',
            finished_at: '2022-12-07T22:52:01.299Z',
            duration: 19.069423,
            queued_duration: 0.60046,
            web_url: 'https://ops.gitlab.net/gitlab-org/release/tools/-/jobs/8694895'),

      build(:job, name: 'job2'),

      build(:job,
            name: 'job3',
            status: 'canceled',
            started_at: nil,
            finished_at: nil,
            web_url: 'https://ops.gitlab.net/gitlab-org/release/tools/-/jobs/8694895')
    ]
  end

  let(:job_attributes) do
    {
      job_name: jobs[0].name,
      id: jobs[0].id,
      web_url: jobs[0].web_url,
      allow_failure: jobs[0].allow_failure,
      user: jobs[0].user.username,
      stage: jobs[0].stage,
      started_at: jobs[0].started_at,
      finished_at: jobs[0].finished_at,
      status: jobs[0].status,
      duration: jobs[0].duration,
      queued_duration: jobs[0].queued_duration
    }
  end

  let(:pipeline) do
    instance_double(
      ReleaseTools::PipelineTracer::Pipeline,
      jobs: Gitlab::PaginatedResponse.new(jobs),
      client: ReleaseTools::GitlabOpsClient
    )
  end

  let(:tracer) { instance_double(OpenTelemetry::Internal::ProxyTracer) }
  let(:span) { instance_double(OpenTelemetry::Trace::Span, finish: nil) }
  let(:job1) { ReleaseTools::PipelineTracer::Job.new(jobs[0], ReleaseTools::GitlabOpsClient) }
  let(:trace_depth) { 2 }

  subject(:execute) { described_class.new(tracer, pipeline, trace_depth: trace_depth).execute }

  before do
    allow(tracer).to receive(:start_span).and_return(span)

    allow(ReleaseTools::PipelineTracer::Job).to receive(:new).and_call_original
    allow(ReleaseTools::PipelineTracer::Job)
      .to receive(:new)
      .with(jobs[0], ReleaseTools::GitlabOpsClient)
      .and_return(job1)
    allow(job1).to receive(:triggered_downstream_pipeline?).and_return(false)
  end

  it 'creates span for each job' do
    expect(tracer)
      .to receive(:start_span)
      .with(
        jobs[0].name,
        {
          links: nil,
          kind: :internal,
          start_timestamp: Time.parse(jobs[0].started_at),
          attributes: job_attributes.stringify_keys
        }
      )

    expect(span).to receive(:finish).with({ end_timestamp: Time.parse(jobs[0].finished_at) })

    execute
  end

  it 'ignores jobs that are not completed' do
    expect(tracer).not_to receive(:start_span).with(jobs[1].name, any_args)

    execute
  end

  it 'ignores jobs where started_at or finished_at is nil' do
    expect(tracer).not_to receive(:start_span).with(jobs[2].name, any_args)

    execute
  end

  it 'does not call triggered_pipeline_url if triggered_downstream_pipeline? is false' do
    expect(job1).to receive(:triggered_downstream_pipeline?)
    expect(job1).not_to receive(:triggered_pipeline_url)
    expect(ReleaseTools::PipelineTracer::Service).not_to receive(:from_pipeline_url)

    execute
  end

  context 'triggered job' do
    before do
      allow(ReleaseTools::PipelineTracer::Job)
        .to receive(:new)
        .with(jobs[0], ReleaseTools::GitlabOpsClient)
        .and_return(job1)
      allow(job1).to receive(:triggered_downstream_pipeline?).and_return(true)
      allow(job1).to receive(:triggered_pipeline_url).and_return('https://url.local')

      allow(ReleaseTools::PipelineTracer::Service).to receive(:from_pipeline_url).and_return(double(execute: nil))
    end

    it 'executes Service' do
      expect(job1).to receive(:triggered_downstream_pipeline?)
      expect(job1).to receive(:triggered_pipeline_url)

      expect(ReleaseTools::PipelineTracer::Service)
        .to receive(:from_pipeline_url)
        .with(
          'https://url.local',
          {
            pipeline_name: "#{jobs[0].name} triggered downstream pipeline",
            trace_depth: trace_depth - 1
          }
        )

      execute
    end
  end

  context 'with retried jobs' do
    let(:span) { instance_double(OpenTelemetry::Trace::Span, finish: nil, context: double) }

    let(:jobs) do
      [
        build(:job,
              :success,
              name: 'job1',
              allow_failure: false,
              started_at: '2022-12-07T22:51:42.229Z',
              finished_at: '2022-12-07T22:52:01.299Z',
              duration: 19.069423,
              queued_duration: 0.60046,
              web_url: 'https://ops.gitlab.net/gitlab-org/release/tools/-/jobs/8694895'),

        build(:job,
              status: 'failed',
              name: 'job1',
              allow_failure: false,
              started_at: '2022-12-07T23:52:41.239Z',
              finished_at: '2022-12-07T23:55:02.399Z',
              duration: 19.069423,
              queued_duration: 0.60046,
              web_url: 'https://ops.gitlab.net/gitlab-org/release/tools/-/jobs/8694896')
      ]
    end

    let(:job2_attributes) do
      {
        job_name: jobs[1].name,
        id: jobs[1].id,
        web_url: jobs[1].web_url,
        allow_failure: jobs[1].allow_failure,
        user: jobs[1].user.username,
        stage: jobs[1].stage,
        started_at: jobs[1].started_at,
        finished_at: jobs[1].finished_at,
        status: jobs[1].status,
        duration: jobs[1].duration,
        queued_duration: jobs[1].queued_duration
      }
    end

    before do
      job2 = ReleaseTools::PipelineTracer::Job.new(jobs[1], ReleaseTools::GitlabOpsClient)
      allow(ReleaseTools::PipelineTracer::Job)
        .to receive(:new)
        .with(jobs[1], ReleaseTools::GitlabOpsClient)
        .and_return(job2)
      allow(job2).to receive(:triggered_downstream_pipeline?).and_return(false)
    end

    it 'adds links' do
      expect(tracer)
        .to receive(:start_span)
        .with(
          jobs[0].name,
          {
            links: nil,
            kind: :internal,
            start_timestamp: Time.parse(jobs[0].started_at),
            attributes: job_attributes.stringify_keys
          }
        )

      expect(tracer)
        .to receive(:start_span)
        .with(
          jobs[1].name,
          {
            links: [OpenTelemetry::Trace::Link.new(span.context)],
            kind: :internal,
            start_timestamp: Time.parse(jobs[1].started_at),
            attributes: job2_attributes.stringify_keys
          }
        )
        .and_return(instance_double(OpenTelemetry::Trace::Span, finish: nil))

      execute
    end
  end
end
